function getRandomItem(arr) {
    let randomIndex = Math.floor(Math.random() * arr.length);
    let item = arr[randomIndex];
    return item;
}

async function flashEmoji(emojiList)  {
    let index = await getRandomItem(emojiList);
    document.getElementById('emoji').innerHTML = index;
}

document.addEventListener("DOMContentLoaded", async function() {
    setInterval(function(){ 
    const emojiList  = ["🔥", "🍀", "🍃", "🍑", "🍒", "🥕", "🥝", "🍏", "🍉", "🌿", "⭐"];
        flashEmoji(emojiList)
    }, 800);
});