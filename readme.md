

# Candleaf 🌻

What is 🌿Candleaf? Candleaf is nothing more and nothing less than a candle sales site. 

Here is the moodboard :

<img src="https://i.imgur.com/DccAP3i.png" style="zoom: 50%;" />

⛔ Here is the current rendering. Keep in mind that the project is still under development and not yet finalized.

<img src="https://i.imgur.com/c1H3JjA.jpg" alt="designe" style="zoom:25%;" />